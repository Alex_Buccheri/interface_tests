#!/usr/bin/env python3
# --------------------------------------------------------------------
# Run DLPOLY v 4.10 for N MD steps, then run standalone DFTB+ v18.2
# for any of the N MD steps and compare the forces
# --------------------------------------------------------------------
import os
import sys
import numpy as np
import shutil
import test_functions as tf

# -------------------------------------
# Main Routine
# -------------------------------------

#Inputs 
MPI_np=2                    # Number of processes
N_MD=5                      # Final MD step         
run_option='forces'        # 'DLPOLY', 'DFTB', 'both' or 'forces'


#Executables
DL_EXE='/home/alex/dl-poly-4.10/build-intel2019-dftb-mpi/bin/DLPOLY.Z'
DFTB_EXE='/home/alex/dftbplus-18.2/build-intel19.0-mpi/dftb+.exe'

#Input and output directories
input_dir={'DLPOLY':'inputs','DFTB':'inputs'}
output_dir={'DLPOLY':'DL_output_'+str(N_MD), 'DFTB':'DFTB_output_'+str(N_MD)}
#MPI_FLAGS = 

# ------------------------------------------------
# Do DLPOLY calculation
# ------------------------------------------------
if run_option=='DLPOLY' or run_option=='both':
    
    #Check for DLPOLY exe
    if os.path.isfile(DL_EXE)==False:
        print('DLPOLY executable needs compiling or path is wrong')
        sys.exit('Script has stopped')

    # Set DLPOLY CONTROL for N MD time steps (before copying to run dir)
    target_line = tf.String_replacement(before='steps', after='', replace=str(N_MD), padding='')
    tf.modify_variable(input_dir['DLPOLY'] + '/CONTROL', input_dir['DLPOLY'] + '/CONTROL', target_line)
    
    #Create run directory
    tf.clean_up_prior_test(output_dir['DLPOLY'])
    tf.mkdir(output_dir['DLPOLY'])
    
    # Copy inputs
    tf.setup_dlpoly_files(input_dir['DLPOLY'], output_dir['DLPOLY'])
    os.chdir(output_dir['DLPOLY'])

    # Execute DLPOLY in run dir and return forces to file
    print('Running DLPOLY with the DFTB+ interface for ', N_MD+1, ' time steps, using '\
          +str(MPI_np)+' processes')
    tf.mpi_run(MPI_np,DL_EXE,mpi_flags=None,output='DFTB_term.out')
    os.chdir('../')


# ------------------------------------------------
# Do N_MD standalone calculations using DFTB+ 
# ------------------------------------------------    
if run_option == 'DFTB' or run_option == 'both':

    #Check DFTB exe exists 
    if os.path.isfile(DFTB_EXE)==False:
        print('DFTB executable needs compiling or path is wrong')
        sys.exit('Script has stopped')

    #Check DLPOLY run directory exists - for structure.gen files 
    if os.path.isdir(output_dir['DLPOLY'])== False:
        print('No DLPOLY run directory from which to take structure files')
        sys.exit('Script has stopped')
    
    #Remove top-level dir if it exists from prior calculation
    tf.clean_up_prior_test(output_dir['DFTB'])
    os.mkdir(output_dir['DFTB'])
    
    # MD loop 
    for iMD in range(0, N_MD + 1):
        #Create subdir for calculation
        run_dir = output_dir['DFTB'] + '/structure_' + str(iMD)
        os.mkdir(run_dir)
        #Add geometry block in dftb_in.hsd copied to run_dir 
        tf.setup_dftb_files(input_dir['DLPOLY'] , run_dir, iMD)
        #Copy structure_iMD.gen from DLPOLY output to run dir
        shutil.copyfile(output_dir['DLPOLY']+'/structure_'+str(iMD)+'_rank0.gen', \
                        run_dir+'/structure_'+str(iMD)+'.gen')

        print('Running standalone DFTB+ V18.2 for time step', iMD, 'of ', N_MD,\
              ', using ', MPI_np,' processes' )
        os.chdir(run_dir)
        tf.mpi_run(MPI_np,DFTB_EXE,mpi_flags=None,output='DFTB_term.out')
        os.chdir('../../')

        
# Compare forces from N MD time steps
if run_option=='forces': 
    print('Comparing forces')
    running_avg = 0.
    running_min = 0.
    running_max = 0.
    output_dl   = output_dir['DLPOLY']
    output_dftb = output_dir['DFTB']
    
    for iMD in range(0,N_MD+1):
        dl_file   = output_dl +'/forces_'+str(iMD)+'.dat'  
        dftb_file = output_dftb + '/structure_' + str(iMD) + '/results.tag'
        
        tf.format_results_file(dftb_file)
        
        dftb_forces = np.loadtxt(dftb_file, skiprows=1)
        dlpoly_forces = np.loadtxt(dl_file, skiprows=1)

        fmin, favg, fmax = tf.evaluate_force_diff(dftb_forces, dlpoly_forces)
        print('Difference in forces for MD time step: ',iMD)
        print('  min, avg, max:', fmin, favg, fmax)

        running_avg+=favg
        running_min+=fmin
        running_max+=fmax

    favg=running_avg/float(N_MD+1)
    fmin=running_min/float(N_MD+1)
    fmax=running_max/float(N_MD+1)
    print('Average over all MD steps. min, avg, max: ',fmin, favg, fmax )



