import os
import sys
import shutil
import subprocess
import numpy as np
import pathlib

# from Modules.dftb_py import hsd
# from Modules.dlpoly_py import dl_classes as dl
# from Modules.conversion_py import DL_DFTB

# ------------------------------------------------------------
# Functions for setting up, modifying and copying input files
# ------------------------------------------------------------


def mkdir(dir_name, verbose=False):
    dir_ = pathlib.Path(dir_name)
    if(dir_.exists()):
        if(verbose == True): print("Directory already exists: ",dir_name)
        return False
    else:
       os.system("mkdir "+dir_name)
       if(verbose == True): print("Created directory: ",dir_name)
       return True 

def delete_dir(dir_name):
    dir_ = pathlib.Path(dir_name)
    if(dir_.exists()):
        os.system("rm -r "+dir_name)
        print("Deleted directory: ",dir_name)
    return


# Clean up from prior runs of this test
def clean_up_prior_test(dir):
    if os.path.isdir(dir)==True:
        shutil.rmtree(dir)
        print('Deleted '+dir+' output directory from prior test run')


# String replacement functions to set up DLPOLY CONTROL file
class String_replacement:
    def __init__(self,before,after,replace,padding):
        self.before=before
        self.after=after
        self.replace=replace
        self.padding=padding

def modify_variable(fin, fout, string):
    finput = fin
    foutput = fout
    output_file_string = ''

    with open(finput, 'r') as input_file:
        for line in input_file:
            if line.isspace() is False:
                if string.before in line:
                    newline = string.before+'  '+string.replace+'\n'
                else:
                    newline = line
            elif line.isspace() is True:
                newline = line
            output_file_string += newline

    input_file.close()
    fid = open(foutput, "w+")
    fid.write(output_file_string)
    fid.close()
    finput = foutput


def setup_dlpoly_files(input_dir,output_dir):
    shutil.copyfile(input_dir+'/CONFIG', output_dir+'/CONFIG')
    shutil.copyfile(input_dir+'/CONTROL', output_dir+'/CONTROL')
    shutil.copyfile(input_dir+'/FIELD', output_dir+'/FIELD')
    shutil.copyfile(input_dir+'/dftb_in.hsd', output_dir+'/dftb_in.hsd')


def copy_structure_files(N_MD,input_dir,output_dir):
    for iMD in range(0,N_MD+1):
        fname = '/structure_'+str(iMD)+'.gen'
        shutil.copyfile(input_dir+fname, output_dir+fname)

        
# DLPOLY won't run if geometry options are set in dftb_in.hsd,
# hence remove them
def remove_geo_from_hsd():
    flag=True
    output_file_string=''
    with open('dftb_in.hsd', 'r') as input_file:
        for line in input_file:
            if 'GenFormat' in line: flag=False
            if flag==True: output_file_string += line
            if '}' in line: flag=True
    input_file.close()

    input_file.close()
    fid = open('dftb_in.hsd', "w+")
    fid.write(output_file_string)
    fid.close()


def mpi_run(MPI_np,EXE,mpi_flags=None,output=None):
   #Same line as a new shell is initialised on each os.system call
    mpi_command = "export OMP_NUM_THREADS=1 && mpirun -np "
    if( mpi_flags != None): mpi_command += mpi_flags+" "
    mpi_command += str(MPI_np)+" "+EXE
    if( output    != None): mpi_command += " > "+output
    os.system(mpi_command)
    return
    
# ------------------------------------------------------------
# Running DLPOLY
# ------------------------------------------------------------
def run_dlpoly(run_dir,exe=None):

    if exe != None:
        if "dl" not in os.environ:
            #print("Need to export ''dl'' shell variable as path/2/dlpoly_ROOT")
            #print("or ensure full path to executable is passed to run_dlpoly")
            command=exe
        else:
            command = '''$dl/''' + exe

    if exe==None:
        if "dl" not in os.environ:
            print("Need to export ''dl'' shell variable as path/2/dlpoly_ROOT")
            print("or ensure full path to executable is passed to run_dlpoly")
        # Executable
        exe='build-dftb/bin/DLPOLY.Z'
        command = '''$dl/''' + exe

    # Check one's in the run directory 
    cwd = os.getcwd()
    if cwd[-len(run_dir):] != run_dir: os.chdir(run_dir)

    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    print(process.communicate())


# ------------------------------------------------------------
# Setting up and running DFTB+ V17
# ------------------------------------------------------------

def setup_dftb_files(source_dir,run_dir,iMD):
    iMD=str(iMD)
    if not os.path.exists(source_dir):
        print('DLPOLY Input directory:',source_dir,' does not exist')
        print('Do a DLPOLY run first to generate the input')
        sys.exit('Script has stopped')

    #Parse DFTB+ input, add geometry block and write that to new location
    geo_string='Geometry = GenFormat {  \n'+  \
               '   <<< "structure_'+iMD+'.gen"  \n'  \
               '}  \n'

    fin = open(source_dir+'/dftb_in.hsd', "r")
    newline=''
    cnt=0
    for line in fin:
        newline+=line
        cnt+=1
        #Hack to add geometry data where I want it (although could go at end of file)
        if cnt==2: newline+=geo_string

    fout=open(run_dir+'/dftb_in.hsd', 'w+')
    fout.write(newline)
    fout.close()
    fin.close()



    # Use DLPOLY to generate DFTB+ geometry input rather than python scripts
    # Read in DLPOLY config, copy data to geometry object and write out in .gen for DFTB+
    # config_string = np.genfromtxt(input_dir+'/CONFIG', dtype=str,  delimiter='\n')
    # config=dl.Config()
    # dl.Config_string_to_data(config_string,config)
    # geo=DL_DFTB.configObject_to_geoObject(config,override_boundary=True)
    # gen_string = hsd.hsd_gen_string_cluster(geo)
    # fid= open(dir+"/structure.gen", "w")
    # fid.write(gen_string[:-1])#Last line is blank so don't write it
    # fid.close()


def run_dftbp(run_dir,exe=None):
    if exe != None:
        # Assumes full path
        if "dftbp" not in os.environ:
            print("Need to export ''dftbp'' shell variable as path/2/dftb+_ROOT")
            print("or ensure full path to executable is passed to run_dftbp")
            command=exe
        # Assumes relative path, with $dftbp in the shell
        else:
            command = '''$dftbp/''' + exe

    if exe==None:
        if "dftbp" not in os.environ:
            print("Need to export ''dftbp'' shell variable as path/2/dftb+_ROOT")
            print("or ensure full path to executable is passed to run_dftbp")
        # Executable
        command = '''$dftbp/_build/prog/dftb+/dftb+ '''

    # Must execute from directory with dftb input files
    cwd = os.getcwd()
    if cwd[-len(run_dir):] != run_dir: os.chdir(run_dir)

    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    print(process.communicate())

#Since DFTB+ v18.2, results.tag contains things in addition to the forces
#Remove these to make the comparison easier 
def format_results_file(filepath):

    #Assume filepath ends in results.tag 
    backup_filepath = filepath[:-11]+'backup_results.tag'
    
    if os.path.isfile(backup_filepath)==True:
        #Calculation already done - return 
        return
    elif os.path.isfile(backup_filepath)==False:
        shutil.copyfile(filepath,backup_filepath)
        fin = open(filepath)
        lines = fin.readlines()
        natms = int( lines[4].split(',')[1] )
        newlines=''
        #Write forces block back to file (excluding rest_
        for i in range(4,4+natms+1):
            newlines += ''.join(lines[i])    
        fin.close()
        fout=open(filepath, 'w+')
        fout.write(newlines)
        fout.close()
        return


# ------------------------------------------------------------
# Compare forces returned by both codes
# ------------------------------------------------------------
def evaluate_force_diff(dftb_forces,dlpoly_forces):
    diff_forces = dftb_forces - dlpoly_forces
    avg_diff_forces = np.sum(abs(diff_forces)) / diff_forces.size
    return np.amin(abs(diff_forces)), avg_diff_forces, np.amax(abs(diff_forces))
